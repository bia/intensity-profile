package plugins.fab.intensityprofile;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ui.RectangleAnchor;
import org.jfree.chart.ui.TextAnchor;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import icy.file.FileUtil;
import icy.gui.frame.IcyFrame;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.main.ActiveSequenceListener;
import icy.gui.main.ActiveViewerListener;
import icy.gui.util.GuiUtil;
import icy.gui.viewer.Viewer;
import icy.gui.viewer.ViewerEvent;
import icy.gui.viewer.ViewerEvent.ViewerEventType;
import icy.image.IcyBufferedImage;
import icy.image.IntensityInfo;
import icy.main.Icy;
import icy.plugin.PluginLauncher;
import icy.plugin.PluginLoader;
import icy.plugin.abstract_.PluginActionable;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROIEvent;
import icy.roi.ROIEvent.ROIEventType;
import icy.roi.ROIListener;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import icy.sequence.SequenceEvent;
import icy.sequence.SequenceEvent.SequenceEventSourceType;
import icy.system.thread.ThreadUtil;
import icy.type.collection.array.Array1DUtil;
import icy.util.XLSUtil;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DEllipse;
import plugins.kernel.roi.roi2d.ROI2DLine;
import plugins.kernel.roi.roi2d.ROI2DPolyLine;
import plugins.kernel.roi.roi2d.ROI2DPolygon;
import plugins.kernel.roi.roi2d.ROI2DRectangle;
import plugins.kernel.roi.roi2d.ROI2DShape;

/**
 * A plugin that presents the intensity profile of the selected ROI in 2D. It allows to compute the mean profile through either Z or T axes. If the ROI is 3D it
 * will only compute the mean value of the ROI on each channel.
 * 
 * @author Fabrice De Chaumont
 * @author Daniel Felipe Gonzalez Obando
 */
public class IntensityProfile extends PluginActionable implements ActionListener, ROIListener
{

    IcyFrame mainFrame = new IcyFrame("Intensity profile", true);

    JButton associateROIButton = new JButton("Associate selected ROI");
    JButton exportToExcelButton = new JButton("Export to excel");
    JButton exportToConsoleButton = new JButton("Export to console");
    JCheckBox graphOverZ = new JCheckBox("Graph Z");
    ChartPanel chartPanel;
    JFreeChart chart;
    XYSeriesCollection xyDataset = new XYSeriesCollection();

    JCheckBox OPTION_meanAlongZ = new JCheckBox("Mean along current Z");
    JCheckBox OPTION_meanAlongT = new JCheckBox("Mean along current T");

    ArrayList<Marker> markerDomainList = new ArrayList<Marker>();
    ArrayList<Marker> markerRangeList = new ArrayList<Marker>();

    ROI associatedROI = null;

    @Override
    public void run()
    {

        mainFrame.getContentPane().setLayout(new BoxLayout(mainFrame.getContentPane(), BoxLayout.PAGE_AXIS));

        mainFrame.getContentPane()
                .add(GuiUtil.createLineBoxPanel(associateROIButton, exportToConsoleButton, exportToExcelButton));
        mainFrame.getContentPane().add(GuiUtil.createLineBoxPanel(OPTION_meanAlongZ, OPTION_meanAlongT));

        exportToExcelButton.addActionListener(this);
        OPTION_meanAlongZ.addActionListener(this);
        OPTION_meanAlongT.addActionListener(this);
        exportToConsoleButton.addActionListener(this);

        // Chart

        chart = ChartFactory.createXYLineChart("Intensity profile", "", "intensity", xyDataset,
                PlotOrientation.VERTICAL, true, true, true);
        chartPanel = new ChartPanel(chart, 500, 200, 500, 200, 500, 500, false, false, true, true, true, true);
        mainFrame.getContentPane().add(GuiUtil.createLineBoxPanel(chartPanel));

        // get default ROI

        Sequence activeSequence = getActiveSequence();
        if (activeSequence != null)
        {
            if (activeSequence.getROIs().size() > 0)
            {
                associatedROI = activeSequence.getROIs().get(0);
            }
        }

        if (associatedROI != null)
        {
            associatedROI.addListener(this);
        }

        // update chart

        updateChart();

        // Listeners

        // exportToExcelButton.addActionListener( this );
        associateROIButton.addActionListener(this);

        // finish display

        mainFrame.setVisible(true);
        mainFrame.addToDesktopPane();
        mainFrame.pack();
        mainFrame.center();
        mainFrame.toFront();

        // register events

        Icy.getMainInterface().addActiveSequenceListener(new ActiveSequenceListener()
        {

            @Override
            public void sequenceDeactivated(Sequence sequence)
            {
            }

            @Override
            public void sequenceActivated(Sequence sequence)
            {
            }

            @Override
            public void activeSequenceChanged(SequenceEvent event)
            {
                if (event.getSourceType() == SequenceEventSourceType.SEQUENCE_DATA
                // || event.getSourceType() == SequenceEventSourceType.SEQUENCE_ROI
                )
                {
                    updateChart();
                }

            }
        });

        Icy.getMainInterface().addActiveViewerListener(new ActiveViewerListener()
        {

            @Override
            public void viewerDeactivated(Viewer viewer)
            {
                updateChart();
            }

            @Override
            public void viewerActivated(Viewer viewer)
            {
                updateChart();
            }

            @Override
            public void activeViewerChanged(ViewerEvent event)
            {
                if (event.getType() == ViewerEventType.POSITION_CHANGED)
                {
                    updateChart();
                }
            }
        });

    }

    Runnable updateRunnable;

    private void updateChart()
    {
        chart.setAntiAlias(true);
        chart.setTextAntiAlias(true);

        if (updateRunnable == null)
        {
            updateRunnable = new Runnable()
            {

                @Override
                public void run()
                {

                    updateChartThreaded();
                }
            };
        }
        ThreadUtil.bgRunSingle(updateRunnable);
        // updateRunnable.run(); // non threaded way.
    }

    private void updateChartThreaded()
    {

        // check if ROI still exist in a sequence

        removeAllHorizontalRangeMarker();

        if (associatedROI != null)
        {
            if (associatedROI.getSequences().size() == 0)
            {
                associatedROI = null;
            }
        }

        // create dataSet

        ThreadUtil.invokeNow(new Runnable()
        {

            @Override
            public void run()
            {

                xyDataset.removeAllSeries();

            }
        });

        // check z to display

        int currentZ = 0;
        int currentT = 0;

        Viewer v = Icy.getMainInterface().getFirstViewerContaining(associatedROI);
        if (v != null)
        {
            currentZ = v.getPositionZ();
            currentT = v.getPositionT();
        }

        if (currentZ < 0)
            currentZ = 0; // 3D return -1.
        if (currentT < 0)
            currentT = 0;

        if (associatedROI != null)
        {
            ThreadUtil.invokeNow(new Runnable()
            {

                @Override
                public void run()
                {

                    chart.setTitle("Intensity profile of ROI " + associatedROI.getName());

                }
            });

            // compute chart

            Sequence sequence = associatedROI.getSequences().get(0);
            // Lines and poly-lines are the easiest to handle.
            if (associatedROI instanceof ROI2DLine || associatedROI instanceof ROI2DPolyLine)
            {

                ROI2DShape roiShape = (ROI2DShape) associatedROI;
                ArrayList<Point2D> pointList = roiShape.getPoints();

                computeLineProfile(pointList, currentT, currentZ, sequence);

                if (OPTION_meanAlongZ.isSelected())
                {
                    computeZMeanLineProfile(pointList, currentT, sequence);
                }
                if (OPTION_meanAlongT.isSelected())
                {
                    computeTMeanLineProfile(pointList, currentZ, sequence);
                }

            }
            // For 2D ROIs that have an interior
            else if (associatedROI instanceof ROI2DRectangle || associatedROI instanceof ROI2DEllipse
                    || associatedROI instanceof ROI2DPolygon || associatedROI instanceof ROI2DArea)
            {
                ROI2D roi = (ROI2D) associatedROI;

                List<Point2D> pointList;
                boolean isRectOrPoly = roi instanceof ROI2DRectangle || roi instanceof ROI2DPolygon;

                // Only rectangles and poly have defined control points.
                if (isRectOrPoly)
                {
                    pointList = ((ROI2DShape) roi).getPoints();

                    // The first point and the point direction depends on the drawing order.
                    // Here, we arrange points in ccw order (i.e. cw on the canvas)
                    if (!isCCW(pointList))
                        Collections.reverse(pointList);

                    // Then shift points to set the first point as the min-y min-x
                    shiftToMinYMinXPoint(pointList);

                    // Add the initial point as the final point for the profile to get the final segment.
                    if (pointList != null && !pointList.isEmpty())
                        pointList.add(pointList.get(0));
                }
                // In the case of areas and ellipses, we take all the contour pixels. The first point is the min-y then min-x on the countour. The point order
                // is clockwise on the canvas (i.e. y-axis is inverted).
                else
                {
                    pointList = roi.getBooleanMask2D(-1, currentT, -1, true).getConnectedContourPoints().stream()
                            .map(p -> (Point2D) new Point2D.Double(p.x, p.y)).collect(Collectors.toList());
                }

                // The profile is always drawn. However, control points are only drawn if the ROI is a Rectangle or Polygon
                computeLineProfile(pointList, currentT, currentZ, sequence, isRectOrPoly);

                if (OPTION_meanAlongZ.isSelected())
                {
                    computeZMeanLineProfile(pointList, currentT, sequence);
                }
                if (OPTION_meanAlongT.isSelected())
                {
                    computeTMeanLineProfile(pointList, currentZ, sequence);
                }
            }
            // Finally, if ROI is not on one of the other groups or is 3D
            else
            {
                ROI roi = associatedROI;
                getValueForSurfaceAllComponent(roi, sequence, currentT, currentZ);

                if (OPTION_meanAlongZ.isSelected())
                {
                    computeSurfaceProfileAlongZ(roi, sequence, currentT);
                }

                if (OPTION_meanAlongT.isSelected())
                {
                    computeSurfaceProfileAlongT(roi, sequence, currentZ);
                }
            }

        }

        chart.fireChartChanged();

    }

    private boolean isCCW(List<Point2D> pointList)
    {
        double area = 0d;
        Iterator<Point2D> it = pointList.iterator();
        Point2D prev = null, curr;
        if (it.hasNext())
            prev = pointList.get(pointList.size() - 1);
        while (it.hasNext())
        {
            curr = it.next();
            area += (curr.getX() - prev.getX()) * (curr.getY() + prev.getY());
            prev = curr;
        }
        return area <= 0;
    }

    private void shiftToMinYMinXPoint(List<Point2D> pointList)
    {
        List<Point2D> tempList = new ArrayList<>(pointList);

        int shiftNumber = 0;
        Point2D minPt = tempList.isEmpty() ? null : tempList.get(0), currPt;

        ListIterator<Point2D> it = tempList.listIterator();
        int size = tempList.size();
        for (int i = 0; i < size; i++)
        {
            currPt = it.next();
            if (currPt.getY() < minPt.getY() || (currPt.getY() == minPt.getY() && currPt.getX() < minPt.getX()))
            {
                shiftNumber = i;
                minPt = currPt;
            }
        }

        pointList.clear();
        for (int i = 0, pos = shiftNumber; i < size; i++, pos++)
            pointList.add(tempList.get(pos % size));
    }

    private void getValueForSurfaceAllComponent(ROI roi, Sequence sequence, final int currentT, final int currentZ)
    {

        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            final XYSeries seriesXY = new XYSeries(
                    "Mean of surface channel #" + c + " t:" + currentT + " z:" + currentZ);

            @SuppressWarnings("deprecation")
            IntensityInfo intensityInfo = ROIUtil.getIntensityInfo(sequence, roi, currentZ, currentT, c);
            final double value = intensityInfo.meanIntensity;

            final int channel = c;

            ThreadUtil.invokeNow(new Runnable()
            {

                @Override
                public void run()
                {

                    drawHorizontalSurfaceValue(value, channel, currentT, currentZ);
                    seriesXY.add(0, value);
                    xyDataset.addSeries(seriesXY);

                }
            });
        }

    }

    private void computeSurfaceProfileAlongZ(ROI roi, Sequence sequence, int currentT)
    {

        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            final XYSeries seriesXY = new XYSeries("Mean along Z channel #" + c + " t:" + currentT);
            for (int z = 0; z < sequence.getSizeZ(); z++)
            {
                @SuppressWarnings("deprecation")
                IntensityInfo intensityInfo = ROIUtil.getIntensityInfo(sequence, roi, z, currentT, c);
                double value = intensityInfo.meanIntensity;

                seriesXY.add(z, value);
            }

            ThreadUtil.invokeNow(new Runnable()
            {

                @Override
                public void run()
                {

                    xyDataset.addSeries(seriesXY);

                }
            });

        }

    }

    // private void computeSurfaceProfileAlongT(BooleanMask2D boolMask, Sequence sequence , int currentZ ) {
    private void computeSurfaceProfileAlongT(ROI roi, Sequence sequence, int currentZ)
    {

        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            final XYSeries seriesXY = new XYSeries("Mean along T channel #" + c + " z:" + currentZ);
            for (int t = 0; t < sequence.getSizeT(); t++)
            {
                @SuppressWarnings("deprecation")
                IntensityInfo intensityInfo = ROIUtil.getIntensityInfo(sequence, roi, currentZ, t, c);
                double value = intensityInfo.meanIntensity;

                seriesXY.add(t, value);
            }

            ThreadUtil.invokeNow(new Runnable()
            {

                @Override
                public void run()
                {

                    xyDataset.addSeries(seriesXY);

                }
            });

        }

    }

    // /**
    // * Mean value of the surface
    // */
    // private double getValueForSurface(BooleanMask2D boolMask, IcyBufferedImage image , int component )
    // {
    // double result=0;
    //
    // double[] imageData = Array1DUtil.arrayToDoubleArray( image.getDataXY( component ) , image.isSignedDataType() );
    //
    // int minX = boolMask.bounds.x;
    // int minY = boolMask.bounds.y;
    // int maxX = boolMask.bounds.width + minX ;
    // int maxY = boolMask.bounds.height + minY ;
    //
    // int imageWidth = image.getWidth();
    //
    // int offset = 0;
    // for ( int y = minY ; y< maxY ; y++ )
    // {
    // for ( int x = minX ; x< maxX ; x++ )
    // {
    //
    // if ( boolMask.mask[offset++] == true )
    // {
    // result+= imageData[y*imageWidth+x];
    // }
    //
    // }
    // }
    // if ( offset != 0 )
    // {
    // result /= (double)offset;
    // }
    //
    // return result;
    // }

    private void computeZMeanLineProfile(List<Point2D> pointList, final int currentT, final Sequence sequence)
    {

        if (sequence.getSizeZ() > 1)
        {
            double[][] result = null;
            for (int z = 0; z < sequence.getSizeZ(); z++)
            {
                IcyBufferedImage image = sequence.getImage(currentT, z);

                Profile profile = getValueForPointList(pointList, image);

                if (result == null)
                {
                    result = new double[profile.values.length][profile.values[0].length];
                }

                for (int c = 0; c < sequence.getSizeC(); c++)
                {
                    for (int i = 0; i < profile.values[c].length; i++)
                    {
                        result[c][i] += profile.values[c][i];
                    }
                }
            }
            for (int c = 0; c < sequence.getSizeC(); c++)
            {
                for (int i = 0; i < result[c].length; i++)
                {
                    result[c][i] /= sequence.getSizeZ();
                }
            }

            final double[][] resultCopy = result;

            ThreadUtil.invokeNow(new Runnable()
            {

                @Override
                public void run()
                {

                    for (int c = 0; c < sequence.getSizeC(); c++)
                    {
                        XYSeries seriesXY = new XYSeries("Mean along Z channel #" + c + " t:" + currentT);
                        for (int i = 0; i < resultCopy[c].length; i++)
                        {
                            seriesXY.add(i, resultCopy[c][i]);
                        }
                        xyDataset.addSeries(seriesXY);
                    }
                }
            });
        }

    }

    private void computeTMeanLineProfile(List<Point2D> pointList, final int currentZ, final Sequence sequence)
    {

        if (sequence.getSizeT() > 1)
        {
            double[][] result = null;
            for (int t = 0; t < sequence.getSizeT(); t++)
            {
                // System.out.println("t:" + t + " currentZ: " + currentZ + " " + sequence.getImage( t , currentZ ) );

                IcyBufferedImage image = sequence.getImage(t, currentZ);
                // if ( image == null ) return;
                Profile profile = getValueForPointList(pointList, image);

                if (result == null)
                {
                    result = new double[profile.values.length][profile.values[0].length];
                }

                for (int c = 0; c < sequence.getSizeC(); c++)
                {
                    for (int i = 0; i < profile.values[c].length; i++)
                    {
                        result[c][i] += profile.values[c][i];
                    }
                }
            }
            for (int c = 0; c < sequence.getSizeC(); c++)
            {
                for (int i = 0; i < result[c].length; i++)
                {
                    result[c][i] /= sequence.getSizeT();
                }
            }

            final double[][] resultCopy = result;

            ThreadUtil.invokeNow(new Runnable()
            {

                @Override
                public void run()
                {

                    for (int c = 0; c < sequence.getSizeC(); c++)
                    {
                        XYSeries seriesXY = new XYSeries("Mean along T channel #" + c + " z:" + currentZ);
                        for (int i = 0; i < resultCopy[c].length; i++)
                        {
                            seriesXY.add(i, resultCopy[c][i]);
                        }
                        xyDataset.addSeries(seriesXY);
                    }

                }
            });

        }

    }

    private void computeLineProfile(List<Point2D> pointList, final int currentT, final int currentZ,
            final Sequence sequence)
    {
        computeLineProfile(pointList, currentT, currentZ, sequence, true);
    }

    private void computeLineProfile(List<Point2D> pointList, int currentT, int currentZ, Sequence sequence,
            boolean showControlPoints)
    {
        final Profile profile = getValueForPointList(pointList, sequence.getImage(currentT, currentZ));

        if (showControlPoints)
            SwingUtilities.invokeLater(() -> drawVerticalROIBreakBar(profile));
        else
            SwingUtilities.invokeLater(() -> chart.getXYPlot().clearDomainMarkers());

        for (int c = 0; c < sequence.getSizeC(); c++)
        {
            final XYSeries seriesXY = new XYSeries("Intensity c" + c + " t" + currentT + " z" + currentZ);
            for (int i = 0; i < profile.values[c].length; i++)
            {
                // System.out.println();
                seriesXY.add(i, profile.values[c][i]);
            }

            ThreadUtil.invokeNow(new Runnable()
            {

                @Override
                public void run()
                {

                    xyDataset.addSeries(seriesXY);

                }
            });

        }
    }

    private void drawVerticalROIBreakBar(Profile profile)
    {

        final XYPlot plot = chart.getXYPlot();

        for (Marker marker : markerDomainList)
        {
            plot.removeDomainMarker(marker);
        }

        if (profile.roiLineBreaks.size() <= 1)
            return;

        int nb = 1;
        for (Integer i : profile.roiLineBreaks)
        {
            final Marker start = new ValueMarker(i);
            markerDomainList.add(start);
            start.setPaint(Color.black);
            start.setLabel("" + nb);
            start.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
            start.setLabelTextAnchor(TextAnchor.TOP_LEFT);
            plot.addDomainMarker(start);
            nb++;
        }

    }

    void removeAllHorizontalRangeMarker()
    {
        ThreadUtil.invokeNow(new Runnable()
        {

            @Override
            public void run()
            {

                final XYPlot plot = chart.getXYPlot();
                for (Marker marker : markerRangeList)
                {
                    plot.removeRangeMarker(marker);
                }

            }
        });
    }

    private void drawHorizontalSurfaceValue(final double value, final int c, final int currentT, final int currentZ)
    {

        ThreadUtil.invokeNow(new Runnable()
        {

            @Override
            public void run()
            {

                final XYPlot plot = chart.getXYPlot();

                final Marker start = new ValueMarker(value);
                markerRangeList.add(start);
                start.setLabel("channel #" + c + " t: " + currentT + " z:" + currentZ + " mean value: " + value);

                start.setPaint(Color.black);
                switch (c)
                {
                    case 0:
                        start.setPaint(Color.red);
                        start.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
                        start.setLabelTextAnchor(TextAnchor.TOP_LEFT);
                        break;
                    case 1:
                        start.setPaint(Color.green.darker());
                        start.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
                        start.setLabelTextAnchor(TextAnchor.TOP_LEFT);
                        break;
                    case 2:
                        start.setPaint(Color.blue);
                        start.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
                        start.setLabelTextAnchor(TextAnchor.TOP_LEFT);
                        break;
                }
                start.setLabelPaint(start.getPaint());
                plot.addRangeMarker(start);
            }
        });

    }

    class Profile
    {
        double[][] values;
        ArrayList<Integer> roiLineBreaks = new ArrayList<Integer>();
    }

    private Profile getValueForPointList(List<Point2D> pointList, IcyBufferedImage image)
    {

        ArrayList<double[][]> dataList = new ArrayList<double[][]>();
        ArrayList<Integer> roiLineBreaks = new ArrayList<Integer>();

        int indexSize = 0;

        for (int i = 0; i < pointList.size() - 1; i++)
        {
            double dataTmp[][] = getValueFor1DSegment(pointList.get(i), pointList.get(i + 1), image);
            indexSize += dataTmp[0].length;
            dataList.add(dataTmp);
            roiLineBreaks.add(indexSize);
        }

        double data[][] = new double[image.getSizeC()][indexSize];

        int index = 0;
        for (double[][] dataToAdd : dataList)
        {
            for (int c = 0; c < image.getSizeC(); c++)
            {
                for (int i = 0; i < dataToAdd[0].length; i++)
                {
                    data[c][index + i] = dataToAdd[c][i];
                }
            }
            index += dataToAdd[0].length;
        }

        Profile profile = new Profile();
        profile.values = data;
        profile.roiLineBreaks = roiLineBreaks;
        return profile;

    }

    private double[][] getValueFor1DSegment(Point2D p1, Point2D p2, IcyBufferedImage image)
    {

        if (image == null)
        {
            return null;
        }

        int distance = (int) p1.distance(p2);

        double vx = (p2.getX() - p1.getX()) / (double) distance;
        double vy = (p2.getY() - p1.getY()) / (double) distance;

        int nbComponent = image.getSizeC();
        double[][] data = new double[nbComponent][distance];

        double x = p1.getX();
        double y = p1.getY();

        for (int i = 0; i < distance; i++)
        {
            // IcyBufferedImage image = canvas.getCurrentImage();
            if (image.isInside((int) x, (int) y))
            {
                for (int component = 0; component < nbComponent; component++)
                {
                    data[component][i] = Array1DUtil.getValue(image.getDataXY(component),
                            image.getOffset((int) x, (int) y), image.isSignedDataType());
                }

            }
            else
            {
                for (int component = 0; component < nbComponent; component++)
                {
                    data[component][i] = 0;
                }
            }

            x += vx;
            y += vy;
        }

        return data;

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {

        if (e.getSource() == OPTION_meanAlongT || e.getSource() == OPTION_meanAlongZ)
        {
            updateChart();
        }

        if (e.getSource() == exportToExcelButton)
        {
            exportToExcel();
        }

        if (e.getSource() == exportToConsoleButton)
        {
            exportToConsole();
        }

        if (e.getSource() == associateROIButton)
        {
            // remove previous listener.

            if (associatedROI != null)
            {
                associatedROI.removeListener(this);
            }

            Sequence activeSequence = Icy.getMainInterface().getActiveSequence();
            if (activeSequence != null)
            {
                associatedROI = activeSequence.getSelectedROI();
            }
            // associatedROI = Icy.getMainInterface().getActiveSequence().getSelectedROI();

            if (associatedROI != null)
            {
                associatedROI.addListener(this);
            }

            updateChart();
        }

    }

    private void exportToConsole()
    {

        List<?> xyDataSetList = xyDataset.getSeries();

        if (associatedROI != null)
        {
            System.out.println("------ Intensity Profile console output");
            System.out.println("ROI name: " + associatedROI.getName());
        }

        for (int i = 0; i < xyDataSetList.size(); i++)
        {
            XYSeries series = (XYSeries) xyDataSetList.get(i);

            System.out.println("--- Graph output");
            System.out.println("Description: " + series.getKey().toString());
            System.out.println("nb items: " + series.getItemCount());

            List<?> itemList = series.getItems();

            for (int j = 0; j < itemList.size(); j++)
            {
                XYDataItem item = (XYDataItem) itemList.get(j);
                System.out.println("x : " + item.getXValue() + " y : " + item.getYValue());
            }

        }

    }

    private void exportToExcel()
    {

        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select xls file.");
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal != JFileChooser.APPROVE_OPTION)
            return;

        File file = chooser.getSelectedFile();

        file = new File(FileUtil.setExtension(file.getAbsolutePath(), ".xls"));

        // XLSUtil xlsUtil = new XLSUtil();
        WritableWorkbook workBook;

        try
        {
            workBook = XLSUtil.createWorkbook(file);
        }
        catch (IOException e)
        {
            new AnnounceFrame("Can't create the excel output. (file already opened ?)");
            // e.printStackTrace();
            return;
        }

        WritableSheet writableSheet = workBook.createSheet("Intensity profile", 0);

        List<?> xyDataSetList = xyDataset.getSeries();

        XLSUtil.setCellString(writableSheet, 0, 0, "ROI name:");
        String roiName = "no roi associated";
        if (associatedROI != null)
        {
            roiName = associatedROI.getName();
        }

        XLSUtil.setCellString(writableSheet, 1, 0, roiName);
        XLSUtil.setCellString(writableSheet, 0, 1, "Sequence:");
        XLSUtil.setCellString(writableSheet, 1, 1, getActiveSequence().getName());

        for (int i = 0; i < xyDataSetList.size(); i++)
        {
            XYSeries series = (XYSeries) xyDataSetList.get(i);
            WritableSheet w = workBook.createSheet(series.getKey().toString(), 0);

            XLSUtil.setCellString(w, 0, 0, "x");
            XLSUtil.setCellString(w, 1, 0, "i");

            XLSUtil.setCellString(w, 3, 0, "Serie:");
            XLSUtil.setCellString(w, 4, 0, series.getKey().toString());
            XLSUtil.setCellString(w, 3, 1, "ROI:");
            XLSUtil.setCellString(w, 4, 1, roiName);

            int yOffset = 1;

            List<?> itemList = series.getItems();

            for (int j = 0; j < itemList.size(); j++)
            {
                XYDataItem item = (XYDataItem) itemList.get(j);
                XLSUtil.setCellNumber(w, 0, yOffset, item.getXValue());
                XLSUtil.setCellNumber(w, 1, yOffset, item.getYValue());
                yOffset++;
            }

        }

        try
        {
            XLSUtil.saveAndClose(workBook);
        }
        catch (WriteException e)
        {
            // e.printStackTrace();
            new AnnounceFrame("Can't create the excel output. (file already opened ?)");
            return;
        }
        catch (IOException e)
        {
            // e.printStackTrace();
            new AnnounceFrame("Can't create the excel output. (file already opened ?)");
            return;
        }

        new AnnounceFrame("Excel export finished: " + file.getAbsolutePath());
    }

    @Override
    public void roiChanged(ROIEvent event)
    {

        boolean update = false;
        if (event.getType() == ROIEventType.PROPERTY_CHANGED)
        {
            if (event.getPropertyName().equals(ROI.PROPERTY_NAME))
            {
                update = true;
            }
        }

        if (event.getType() == ROIEventType.ROI_CHANGED)
        {
            update = true;
        }

        if (update)
        {
            updateChart();
        }

    }

    public static void main(String[] args)
    {
        Icy.main(args);
        PluginLauncher.start(PluginLoader.getPlugin(IntensityProfile.class.getName()));
    }

}
